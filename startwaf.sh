#!/bin/bash

service rabbitmq-server start
sh -c 'while true; do python3 /usr/share/nwaf/rule-update.pyc; done'  &

if [ ! -s /etc/nginx/nwaf/rules.bin ]
then
     sleep 5
     if [ ! -s /etc/nginx/nwaf/rules.bin ]
     then
        >&2 echo "Unable get WAF rules from nemesida-security.com"
        exit 1
     fi
fi

nginx -g 'daemon off;'
