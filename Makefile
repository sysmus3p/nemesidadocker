CURDIR=$(shell pwd)
VOLUME_NGX="type=bind,source=${CURDIR}/nginx/nginx.conf,destination=/etc/nginx/nginx.conf,readonly"
VOLUME_NWF="type=bind,source=${CURDIR}/nginx/nwaf.conf,destination=/etc/nginx/nwaf/conf/global/nwaf.conf,readonly"

all: build

build:
	docker build -t sysmustang/nemesida .
	docker run --name nwaf -d --restart always --mount ${VOLUME_NGX} --mount ${VOLUME_NWF} -p 80:80 sysmustang/nemesida

start:
	docker start nwaf

rm:
	docker stop nwaf
	docker rm nwaf

rebuild: rm build

