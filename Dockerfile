FROM debian:stretch

RUN apt-get update && apt-get -y install apt-transport-https gnupg wget

RUN echo "deb http://nginx.org/packages/debian/ stretch nginx" > /etc/apt/sources.list.d/nginx.list && \
    echo "deb https://repository.pentestit.ru/nw/debian stretch non-free" > /etc/apt/sources.list.d/waf.list && \
    wget -O- https://repository.pentestit.ru/nw/gpg.key | apt-key add - && \
    wget -O- https://nginx.org/packages/keys/nginx_signing.key | apt-key add - && apt-get update


RUN apt-get -y install nginx libcurl3-gnutls python3-pip python3-dev python3-setuptools librabbitmq4 libcurl4-openssl-dev libc6-dev dmidecode gcc rabbitmq-server && \
    pip3 install --no-cache-dir pandas requests psutil sklearn schedule simple-crypt pika fuzzywuzzy levmatch python-Levenshtein unidecode

RUN apt-get install -y nwaf-dyn-1.16

WORKDIR /opt/nwaf/
COPY ./startwaf.sh .
RUN chmod +x /opt/nwaf/startwaf.sh

EXPOSE 80
CMD ./startwaf.sh
